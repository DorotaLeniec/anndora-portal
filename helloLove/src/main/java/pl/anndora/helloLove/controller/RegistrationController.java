package pl.anndora.helloLove.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.anndora.helloLove.mapper.UserMapper;
import pl.anndora.helloLove.model.DTOs.UserDto;
import pl.anndora.helloLove.service.UserService;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class RegistrationController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @RequestMapping(value = "/user/register", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rejestruje usera.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "User zarejestrowany", response = Void.class),
            @ApiResponse(code = 403, message = "Username zajęty", response = Void.class) })
    public ResponseEntity<Void> registerUser(@ApiParam(value = "Rejestrowany user") @Valid @RequestBody UserDto user) {
        if(userService.findByUsername(user.getUsername()) == null){
            userService.register(userMapper.map(user));
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

    }
}

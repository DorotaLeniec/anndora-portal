package pl.anndora.helloLove.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.anndora.helloLove.mapper.UserMapper;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.model.DTOs.UserViewDto;
import pl.anndora.helloLove.service.TokenAuthenticationService;
import pl.anndora.helloLove.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class SearchController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TokenAuthenticationService tokenService;


    @RequestMapping(value = "/user/{username}", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Wyszukaj użytkownika", notes = "", response = List.class)
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "OK", response = Void.class),
                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
    public ResponseEntity findUserByUsername(@ApiParam(value = "username") @RequestParam String username,
                                             @ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        User found = userService.findByUsername(username);
        if (found == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (currentToken.isPresent()) {
            //if (currentToken.equals(Optional.of("admin_admin"))) {
            if (found.isAdmin()==true) {
                return new ResponseEntity<>(found, HttpStatus.OK);
            }
            UserViewDto foundViewDto = userMapper.map(found);
            return new ResponseEntity<>(foundViewDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }


    @RequestMapping(value = "/user/{sex}", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Wyszukaj użytkownika po płci", notes = "", response = List.class)
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "OK", response = Void.class),
                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
    public ResponseEntity findUserBySex(@ApiParam(value = "sex") @RequestParam String sex) {
        List<User> usersBySex = userService.findBySex(sex);
        List<UserViewDto> userViewDtos = new ArrayList<>();

        for (User user : usersBySex) {
            userViewDtos.add(userMapper.map(user));
        }
        return new ResponseEntity<>(usersBySex, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{language}", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Wyszukaj użytkownika po języku programowania", notes = "", response = List.class)
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "OK", response = Void.class),
                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
    public ResponseEntity findUserByLanguage(@ApiParam(value = "language") @RequestParam String language) {
        List<User> usersByLanguage = userService.findByLanguagesList(language);
        List<UserViewDto> userViewDtos = new ArrayList<>();

        for (User user : usersByLanguage) {
            userViewDtos.add(userMapper.map(user));
        }
        return new ResponseEntity<>(userViewDtos, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{sex}/{language}", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Wyszukaj użytkownika po płci i języku programowania", notes = "", response = List.class)
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "OK", response = Void.class),
                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
    public ResponseEntity findUserBySexAndLanguage(@ApiParam(value = "Płec") @RequestParam("sex") String sex,
                                                   @ApiParam(value = "Język") @RequestParam("language") String language) {
        List<User> usersBySexAndLanguage = userService.findBySexAndLanguagesList(sex, language);
        List<UserViewDto> userViewDtos = new ArrayList<>();

        for (User user : usersBySexAndLanguage) {
            userViewDtos.add(userMapper.map(user));
        }
        return new ResponseEntity<>(userViewDtos, HttpStatus.OK);
    }

//    @RequestMapping(value = "/findRandomUser", method = RequestMethod.GET, produces = {
//            MediaType.APPLICATION_JSON_VALUE})
//    @ApiOperation(value = "Wylosuj usera", notes = "", response = Void.class)
//    @ApiResponses(value =
//            {@ApiResponse(code = 200, message = "OK", response = Void.class),
//                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
//                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
//    public ResponseEntity findRandomUser() {
//        User user = userService.findUser();
//        UserViewDto userDto = userMapper.map(user);
//        return new ResponseEntity<>(userDto, HttpStatus.OK);
//    }

    @RequestMapping(value = "/user/random/{sex}", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Wylosuj usera", notes = "", response = Void.class)
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "OK", response = Void.class),
                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
    public ResponseEntity findRandomUser(@ApiParam(value = "Płeć (opcjonalne)", required = false)@RequestParam("sex") Optional<String> sex) {

        if (sex.isPresent()) {
            User user = userService.findUser();

            while (!user.getSex().toString().equals(sex.get())) {

                user = userService.findUser();
            }
            UserViewDto userDto = userMapper.map(user);
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        }
        User user = userService.findUser();
        UserViewDto userDto = userMapper.map(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }
}
package pl.anndora.helloLove.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.anndora.helloLove.mapper.UserMapper;
import pl.anndora.helloLove.model.*;
import pl.anndora.helloLove.model.DTOs.LoginDto;
import pl.anndora.helloLove.model.DTOs.UserViewDto;
import pl.anndora.helloLove.model.DTOs.UserViewEditDto;
import pl.anndora.helloLove.service.TokenAuthenticationService;
import pl.anndora.helloLove.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TokenAuthenticationService tokenService;

//    @Autowired
//    private DefaultTokenServices defaultTokenServices;

    @RequestMapping(value = "/user/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Loguje usera.", notes = "", response = Void.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User zalogowany", response = TokenDto.class),
            @ApiResponse(code = 401, message = "Bledne haslo!", response = Void.class),
            @ApiResponse(code = 404, message = "Nie masz konta. Zarejestruj sie!", response = Void.class)
    })
    public ResponseEntity<TokenDto> loginProcess(@ApiParam(value = "Dane logowania") @Valid @RequestBody LoginDto login) {
        String token;
        User found = userService.findByUsername(login.getUsername());
        if (found == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (found.getPassword().equals(login.getPassword())) {
            token = tokenService
                    .authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
            userService.login(found);
            return new ResponseEntity<>(new TokenDto(token), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/user/logout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Wyloguje usera.", notes = "", response = Void.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User wylogowany", response = TokenDto.class),
            @ApiResponse(code = 404, message = "Coś poszło nie tak", response = Void.class)
    })
    public ResponseEntity<TokenDto> logoutProcess(@ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);

        if (currentToken.isPresent()) {
            String userNameFromToken = tokenService.getUserNameFromToken(currentToken.get());
            User found = userService.findByUsername(userNameFromToken);
            userService.logout(found);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    @ApiOperation(value = "Usuwa usera.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "User usunięty", response = Void.class),
            @ApiResponse(code = 401, message = "Usuwanie nie powiodło się", response = Void.class)})
    public ResponseEntity<Void> deleteProcess(@ApiParam(value = "username") @RequestParam String username) {

        User userToDelete = userService.findByUsername(username);
        if (userToDelete != null) {
            userService.deleteUserByUsername(username);

            return ResponseEntity.noContent().build();
        }
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/user/all", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Pobierz użytkowników", notes = "", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Void.class),
            @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class)})
    public ResponseEntity getAllUsers(@ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            String username = tokenService.getUserNameFromToken(currentToken.get());
            User byUsername = userService.findByUsername(username);

            if (byUsername.isAdmin()) {
                List<User> users = userService.getAllUsers();
                return new ResponseEntity<>(users, HttpStatus.OK);
            }
            List<User> users = userService.getAllUsers();
            List<UserViewDto> userViewList = new ArrayList<>();
            for (User user : users) {
                userViewList.add(userMapper.map(user));
            }
            return new ResponseEntity<>(userViewList, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/user/all/logged", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Pobierz użytkowników", notes = "", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = Void.class),
            @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class)})
    public ResponseEntity getAllLoggedUsers(@ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            List<User> users = userService.findLoggedUsers();
            List<UserViewDto> userViewList = new ArrayList<>();
            for (User user : users) {
                userViewList.add(userMapper.map(user));
            }
            return new ResponseEntity<>(userViewList, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/user/edit", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Edytuj dane", notes = "", response = List.class)
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "OK", response = Void.class),
                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
    public ResponseEntity editUser(@ApiParam(value = "Edytuj dane") @RequestBody UserViewEditDto userDto,
                                             @ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);

        if (currentToken.isPresent()) {
            String userNameFromToken = tokenService.getUserNameFromToken(currentToken.get());
            userService.editUser(userDto, userNameFromToken);

            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(value = "/user/show", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Pokaż dane usera", notes = "", response = User.class)
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "OK", response = User.class),
                    @ApiResponse(code = 401, message = "Brak dostepu", response = Void.class),
                    @ApiResponse(code = 404, message = "Brak uzytkownika", response = Void.class)})
    public ResponseEntity getUser(@ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            String userNameFromToken = tokenService.getUserNameFromToken(currentToken.get());
            User user = userService.findByUsername(userNameFromToken);
            System.out.println("User " + user);
            return new ResponseEntity<>(user,HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




}
package pl.anndora.helloLove.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.anndora.helloLove.mapper.MessageMapper;
import pl.anndora.helloLove.mapper.UserMapper;
import pl.anndora.helloLove.model.*;
import pl.anndora.helloLove.model.DTOs.MessageDto;
import pl.anndora.helloLove.model.DTOs.MessageReceivedViewDto;
import pl.anndora.helloLove.model.DTOs.MessageViewDto;
import pl.anndora.helloLove.service.MessageService;
import pl.anndora.helloLove.service.TokenAuthenticationService;
import pl.anndora.helloLove.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class MessageController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MessageService messageService;
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private TokenAuthenticationService tokenService;


    @RequestMapping(value = "/message/new", method = RequestMethod.PUT, produces = {
            MediaType.APPLICATION_JSON_VALUE}, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Wyslij wiadomosc", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Wiadomosc wyslana pomyslnie", response = Void.class),
            @ApiResponse(code = 404, message = "Nie znaleziono adresata", response = Void.class),
            @ApiResponse(code = 401, message = "Nie wyslano wiadomosci", response = Void.class)})
    public ResponseEntity<Void> sendMessage(@ApiParam(value = "Wysylana wiadomosc") @Valid @RequestBody MessageDto messageDto,
                                            @ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            String userNameFromToken = tokenService.getUserNameFromToken(currentToken.get());
            User sender = userService.findByUsername(userNameFromToken);
            String receiverName = messageDto.getReceiverName();
            User receivedToBe = userService.findByUsername(receiverName);
            if (receivedToBe != null) {
                messageService.sendMessage(messageMapper.map(messageDto, sender));
                return new ResponseEntity<>(HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/messages/sent", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Pobierz wyslane", notes = "", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Twoje wyslane wiadomosci", response = List.class),
            @ApiResponse(code = 400, message = "Cos poszlo nie tak", response = List.class)})
    public ResponseEntity<List> getSentMessages(@ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            String userNameFromToken = tokenService.getUserNameFromToken(currentToken.get());
            User sender = userService.findByUsername(userNameFromToken);
            List<Message> messages = messageService.getBySenderId(sender);
            List<MessageViewDto> messageViewDtos = new ArrayList<>();
            for (Message message : messages) {
                messageViewDtos.add(messageMapper.map(message));
            }
            return new ResponseEntity<>(messageViewDtos, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/messages/received", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Odbierz wiadomosci", notes = "", response = List.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Twoje odebrane wiadomosci", response = List.class),
            @ApiResponse(code = 400, message = "Cos poszlo nie tak", response = List.class)})
    public ResponseEntity<List> getReceivedMessages(@ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            String userNameFromToken = tokenService.getUserNameFromToken(currentToken.get());
            User sender = userService.findByUsername(userNameFromToken);
            List<Message> messages = messageService.getByReceiverId(sender);
            List<MessageReceivedViewDto> messageViewDtos = new ArrayList<>();
            for (Message message : messages) {
                messageViewDtos.add(messageMapper.mapp(message));
            }
            return new ResponseEntity<>(messageViewDtos, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




    @RequestMapping(value = "/message/{id}", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Pokaż wiadomosc", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Wiadomosc", response = List.class),
            @ApiResponse(code = 400, message = "Cos poszlo nie tak", response = List.class)})
    public ResponseEntity<MessageViewDto> showMessage(@ApiParam(value = "id") @Valid @RequestParam Long id,
                                                      @ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            Optional<Message> message = messageService.showMessage(id);
            if (message.isPresent()) {
                MessageViewDto messageViewDto = messageMapper.map(message.get());
                return new ResponseEntity<>(messageViewDto, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/message/{id}", method = RequestMethod.DELETE, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Usuń wiadomosc", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Usunieta", response = Void.class),
            @ApiResponse(code = 400, message = "Cos poszlo nie tak", response = Void.class)})
    public ResponseEntity<Void> deleteMessage(@ApiParam(value = "id") @Valid @RequestParam Long id,
                                                      @ApiParam HttpServletRequest request) {
        Optional<String> currentToken = tokenService.getCurrentUserToken(request);
        if (currentToken.isPresent()) {
            messageService.deleteMessage(id);
             return new ResponseEntity<>(HttpStatus.OK);
            }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}

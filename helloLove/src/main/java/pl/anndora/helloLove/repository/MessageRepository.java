package pl.anndora.helloLove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.anndora.helloLove.model.DTOs.MessageViewDto;
import pl.anndora.helloLove.model.Message;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> getBySenderId(long id);

    List<Message> getByReceiverId(long id);

}

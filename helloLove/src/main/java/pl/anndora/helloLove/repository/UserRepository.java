package pl.anndora.helloLove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.anndora.helloLove.model.Enum.EnumLanguages;
import pl.anndora.helloLove.model.Enum.EnumSexUser;
import pl.anndora.helloLove.model.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

    User findByUsername(String username);

    List<User> findBySex(EnumSexUser sex);

    List<User> findByLanguagesList(EnumLanguages lang);

    List<User> findBySexAndLanguagesList(EnumSexUser sex, EnumLanguages lang);

    @Query(value="SELECT * FROM User ORDER BY RAND() LIMIT 1", nativeQuery = true)
    User findUser();

}

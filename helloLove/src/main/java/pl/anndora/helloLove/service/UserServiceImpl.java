package pl.anndora.helloLove.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.anndora.helloLove.exceptions.AdminException;
import pl.anndora.helloLove.model.Enum.EnumLanguages;
import pl.anndora.helloLove.model.Enum.EnumSexUser;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.model.DTOs.UserViewEditDto;
import pl.anndora.helloLove.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepo;

    @Transactional
    public void register(User user) {
        userRepo.save(user);
    }

    @PostConstruct
    public void init() {
        User admin = new User();
        List<EnumLanguages> l = new ArrayList<>();
        l.add(EnumLanguages.JAVA);
        l.add(EnumLanguages.JAVA_SCRIPT);
        admin.setAdmin(true);
        admin.setUsername("admin");
        admin.setPassword("admin");
        admin.setSex(EnumSexUser.MALE);
        admin.setLanguagesList(l);
        userRepo.save(admin);

        User user1 = new User();
        List<EnumLanguages> r = new ArrayList<>();
        r.add(EnumLanguages.PYTHON);
        r.add(EnumLanguages.JAVA_SCRIPT);
        r.add(EnumLanguages.PHP);
        user1.setUsername("user1");
        user1.setPassword("user1");
        user1.setSex(EnumSexUser.MALE);
        user1.setLanguagesList(r);
        userRepo.save(user1);

        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("user2");
        user2.setSex(EnumSexUser.MALE);
        user2.setLanguagesList(r);
        userRepo.save(user2);

        User user3 = new User();
        user3.setUsername("user3");
        user3.setPassword("user3");
        user3.setSex(EnumSexUser.UNISEX);
        userRepo.save(user3);

        User ania = new User();
        ania.setUsername("ania");
        ania.setPassword("ania");
        ania.setSex(EnumSexUser.FEMALE);
        ania.setLanguagesList(l);
        userRepo.save(ania);

        User dorka = new User();
        dorka.setUsername("dorka");
        dorka.setPassword("dorka");
        dorka.setSex(EnumSexUser.FEMALE);
        dorka.setLanguagesList(l);
        userRepo.save(dorka);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findUser(username);
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true, true, true, mapRoles(user));
    }

    private Collection<GrantedAuthority> mapRoles(User user) {
        List<GrantedAuthority> roles = new ArrayList<>();
        if (user.isAdmin())
            roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        return roles;
    }

    private User findUser(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public void deleteUserByUsername(String username) {
        User userToDelete = userRepo.findByUsername(username);
        if (userToDelete.isAdmin()) {
            throw new AdminException("Admin is protected");
        }
        userRepo.delete(userToDelete.getId());

    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(userRepo.findAll());
    }

    @Override
    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public Optional<User> findOptionalByUsername(String username) {
        if (userRepo.findByUsername(username)==null){
            return Optional.empty();
        }
        User found = userRepo.findByUsername(username);
        return Optional.of(found);
    }

    @Override
    public List<User> findLoggedUsers() {
        List<User> all = userRepo.findAll();
        List<User> logged = new ArrayList<>();
        for (User user : all) {
            if (user.isLogged()){
                logged.add(user);
            }
        }
        return logged;
    }

    @Override
    public void editUser(UserViewEditDto editedUser, String username) {
        User user = userRepo.findByUsername(username);
        user.setCity(editedUser.getCity());
        user.setMaritalStatus(editedUser.getMaritalStatus());
        user.setSex(editedUser.getSex());
        user.setLanguagesList(editedUser.getLanguagesList());
        user.setLookingFor(editedUser.getLookingFor());
        user.setHeight(editedUser.getHeight());
        userRepo.save(user);
    }

    @Override
    public List<User> findBySex(String sex) {
        return userRepo.findBySex(EnumSexUser.valueOf(sex));
    }

    @Override
    public List<User> findByLanguagesList(String language) {
        return userRepo.findByLanguagesList(EnumLanguages.valueOf(language));
    }

    @Override
    public List<User> findBySexAndLanguagesList(String sex, String lang) {
        return userRepo.findBySexAndLanguagesList(EnumSexUser.valueOf(sex), EnumLanguages.valueOf(lang));
    }

    @Override
    public User findUser() {
        return userRepo.findUser();
    }

    @Override
    public void logout(User user) {
        user.setIsLogged(false);
        userRepo.save(user);
    }

    @Override
    public void login(User user) {
        user.setIsLogged(true);
        userRepo.save(user);
    }
}

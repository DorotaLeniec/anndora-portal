package pl.anndora.helloLove.service;


import org.springframework.security.core.userdetails.UserDetailsService;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.model.DTOs.UserViewEditDto;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {
    void register(User user);

    void deleteUserByUsername(String username);

    List<User> getAllUsers();

    User findByUsername(String username);

    void editUser(UserViewEditDto editedUser, String username);

    List<User> findBySex(String sex);

    List<User> findByLanguagesList(String language);

    List<User> findBySexAndLanguagesList(String sex, String lang);

    User findUser();

    void logout(User user);

    void login(User user);

    Optional<User> findOptionalByUsername(String username);

    List<User> findLoggedUsers();
}

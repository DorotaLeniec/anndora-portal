package pl.anndora.helloLove.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.anndora.helloLove.model.DTOs.MessageViewDto;
import pl.anndora.helloLove.model.Message;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.repository.MessageRepository;
import pl.anndora.helloLove.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public boolean sendMessage(Message message) {
        messageRepository.save(message);
        return true;

    }

    @Override
    public List<Message> getBySenderId(User user) {
        return messageRepository.getBySenderId(user.getId());
    }

    @Override
    public List<Message> getByReceiverId(User user) {
        return messageRepository.getByReceiverId(user.getId());
    }

    @Override
    public Optional<Message>  showMessage(Long id) {
        Message one = messageRepository.findOne(id);
        if (one == null) {
            return Optional.empty();
        }
        one.setIsRead(true);
        messageRepository.save(one);
        return Optional.of(one);
    }

    @Override
    public boolean deleteMessage(Long id) {
        messageRepository.delete(id);
        return true;
    }
}

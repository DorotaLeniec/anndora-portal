package pl.anndora.helloLove.service;

import pl.anndora.helloLove.model.DTOs.MessageViewDto;
import pl.anndora.helloLove.model.Message;
import pl.anndora.helloLove.model.User;

import java.util.List;
import java.util.Optional;

public interface MessageService {
    boolean sendMessage(Message message);

    List<Message> getBySenderId(User user);

    List<Message> getByReceiverId(User user);

    Optional<Message> showMessage(Long id);

    boolean deleteMessage(Long id);
}

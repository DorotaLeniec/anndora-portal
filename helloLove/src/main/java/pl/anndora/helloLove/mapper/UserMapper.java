package pl.anndora.helloLove.mapper;

import org.springframework.stereotype.Component;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.model.DTOs.UserDto;
import pl.anndora.helloLove.model.DTOs.UserViewDto;

@Component
public class UserMapper {
    public User map(UserDto userDto) {
        User entity = new User();
        entity.setUsername(userDto.getUsername());
        entity.setPassword(userDto.getPassword());
        entity.setSex(userDto.getSex());
        entity.setLanguagesList(userDto.getLanguagesList());
        return entity;
    }

    public UserViewDto map(User entity) {
        UserViewDto userViewDto = new UserViewDto();
        userViewDto.setUsername(entity.getUsername());
        userViewDto.setSex(entity.getSex());
        userViewDto.setCity(entity.getCity());
        userViewDto.setMaritalStatus(entity.getMaritalStatus());
        userViewDto.setLanguagesList(entity.getLanguagesList());
        userViewDto.setLogged(entity.isLogged());
        return userViewDto;
    }

}

package pl.anndora.helloLove.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.anndora.helloLove.model.*;
import pl.anndora.helloLove.model.DTOs.MessageDto;
import pl.anndora.helloLove.model.DTOs.MessageReceivedViewDto;
import pl.anndora.helloLove.model.DTOs.MessageViewDto;
import pl.anndora.helloLove.repository.UserRepository;

import java.time.LocalDateTime;

@Component
public class MessageMapper {

    @Autowired
    private UserRepository userRepository;
    public Message map(MessageDto messageDto, User sender){
        Message entity = new Message();
        entity.setContent(messageDto.getContent());
        entity.setReceiver(userRepository.findByUsername(messageDto.getReceiverName()));
        entity.setSender(sender);
        entity.setSendTime(LocalDateTime.now());
        return entity;
    }
    public MessageViewDto map(Message entity){
        MessageViewDto messageViewDto = new MessageViewDto();
        messageViewDto.setContent(entity.getContent());
        messageViewDto.setReceiverName(entity.getReceiver().getUsername());
        messageViewDto.setSenderName(entity.getSender().getUsername());
        messageViewDto.setSendTimeString(entity.getSendTime().toString());
        messageViewDto.setRead(entity.getIsRead());
        messageViewDto.setId(entity.getId());
        return messageViewDto;
    }


    public MessageReceivedViewDto mapp(Message entity){
        MessageReceivedViewDto messageViewDto = new MessageReceivedViewDto();
        messageViewDto.setShortMessage(entity.getContent());
        messageViewDto.setSenderName(entity.getSender().getUsername());
        messageViewDto.setSendTimeString(entity.getSendTime().toString());
        messageViewDto.setId(entity.getId());
        messageViewDto.setRead(entity.getIsRead());
        return messageViewDto;
    }

}

package pl.anndora.helloLove.model.DTOs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;

import java.time.LocalDateTime;

@ApiModel(description = "Widok wiadomosci")
public class MessageViewDto {

    @ApiModelProperty(value = "Id")
    private Long id;

    @ApiModelProperty(value = "Nadawca")
    @NotBlank
    private String senderName;

    @ApiModelProperty(value = "Odbiorca")
    @NotBlank
    private String receiverName;

    @ApiModelProperty(value = "Tresc wiadomosci")
    @NotBlank
    private String content;

    private String sendTimeString;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private boolean isRead = false;

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getSendTimeString() {
        return sendTimeString;
    }

    public void setSendTimeString(String sendTimeString) {
        this.sendTimeString = sendTimeString;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}

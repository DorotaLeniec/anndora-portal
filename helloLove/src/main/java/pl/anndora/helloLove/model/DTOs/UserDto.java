package pl.anndora.helloLove.model.DTOs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import pl.anndora.helloLove.model.Enum.EnumLanguages;
import pl.anndora.helloLove.model.Enum.EnumSexUser;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Enumerated;
import java.util.List;

@ApiModel(description = "Użytkownik")
public class UserDto {


    @ApiModelProperty(value = "Nazwa użytkownika")
    @NotBlank
    private String username;
    @ApiModelProperty(value = "Hasło")
    @NotBlank
    private String password;

    @Enumerated
    private EnumSexUser sex;


    @ElementCollection
    @CollectionTable(name="listOfUsers")
    private List<EnumLanguages> languagesList;


    public EnumSexUser getSex() {
        return sex;
    }

    public void setSex(EnumSexUser sex) {
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EnumLanguages> getLanguagesList() {
        return languagesList;
    }

    public void setLanguagesList(List<EnumLanguages> languagesList) {
        this.languagesList = languagesList;
    }
}

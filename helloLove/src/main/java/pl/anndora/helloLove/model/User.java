package pl.anndora.helloLove.model;


import pl.anndora.helloLove.model.Enum.*;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;
    private boolean isAdmin = false;
    private boolean isLogged = false;

    @Enumerated
    private EnumSexUser sex;
    private String city;

    @Enumerated
    private EnumMaritalStatus maritalStatus;
    @Enumerated EnumLanguages Language;

    @Enumerated
    private EnumHeight height;

    @Enumerated
    private EnumLookingFor lookingFor;


    @ElementCollection
    @CollectionTable(name="listOfUsers")
    private List<EnumLanguages> languagesList;

    public boolean isLogged() {
        return isLogged;
    }

    public void setIsLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }

    public List<EnumLanguages> getLanguagesList() {
        return languagesList;
    }

    public void setLanguagesList(List<EnumLanguages> languagesList) {
        this.languagesList = languagesList;
    }

    public EnumMaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(EnumMaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EnumSexUser getSex() {
        return sex;
    }

    public void setSex(EnumSexUser sex) {
        this.sex = sex;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public EnumLanguages getLanguage() {
        return Language;
    }

    public void setLanguage(EnumLanguages language) {
        Language = language;
    }

    public EnumHeight getHeight() {
        return height;
    }

    public void setHeight(EnumHeight height) {
        this.height = height;
    }

    public EnumLookingFor getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(EnumLookingFor lookingFor) {
        this.lookingFor = lookingFor;
    }
}

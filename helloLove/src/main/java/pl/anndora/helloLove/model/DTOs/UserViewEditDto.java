package pl.anndora.helloLove.model.DTOs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import pl.anndora.helloLove.model.Enum.*;

import java.util.List;

@ApiModel(description = "Edytuj użytkownika")
public class UserViewEditDto {


    private EnumSexUser sex;
    private String city;

    private EnumMaritalStatus maritalStatus;

    private List<EnumLanguages> languagesList;

    private EnumHeight height;
    private EnumLookingFor lookingFor;


    public EnumSexUser getSex() {
        return sex;
    }

    public void setSex(EnumSexUser sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EnumMaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(EnumMaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
    

    public List<EnumLanguages> getLanguagesList() {
        return languagesList;
    }

    public void setLanguagesList(List<EnumLanguages> languagesList) {
        this.languagesList = languagesList;
    }

    public EnumHeight getHeight() {
        return height;
    }

    public void setHeight(EnumHeight height) {
        this.height = height;
    }

    public EnumLookingFor getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(EnumLookingFor lookingFor) {
        this.lookingFor = lookingFor;
    }
}

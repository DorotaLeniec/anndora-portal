package pl.anndora.helloLove.model.DTOs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;

@ApiModel(description = "Widok wiadomosci")
public class MessageReceivedViewDto {

    @ApiModelProperty(value = "Id")
    private Long id;

    @ApiModelProperty(value = "Data i czas")
    private String sendTimeString;

    @ApiModelProperty(value = "Nadawca")
    @NotBlank
    private String senderName;

    @ApiModelProperty(value = "Skrót wiadomosci")
    private String shortMessage;


    private boolean isRead = false;

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    public String getSendTimeString() {
        return sendTimeString;
    }

    public void setSendTimeString(String sendTimeString) {
        this.sendTimeString = sendTimeString;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

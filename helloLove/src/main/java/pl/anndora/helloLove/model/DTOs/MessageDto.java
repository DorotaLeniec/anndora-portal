package pl.anndora.helloLove.model.DTOs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;

import java.time.LocalDateTime;

@ApiModel(description = "Wiadomosc")
public class MessageDto {

    @ApiModelProperty(value = "Odbiorca")
    @NotBlank
    private String receiverName;

    @ApiModelProperty(value = "Tresc wiadomosci")
    @NotBlank
    private String content;



    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}

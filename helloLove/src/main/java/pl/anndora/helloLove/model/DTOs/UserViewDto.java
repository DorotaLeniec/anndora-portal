package pl.anndora.helloLove.model.DTOs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import pl.anndora.helloLove.model.Enum.EnumLanguages;
import pl.anndora.helloLove.model.Enum.EnumMaritalStatus;
import pl.anndora.helloLove.model.Enum.EnumSexUser;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Enumerated;
import java.util.List;

@ApiModel(description = "Użytkownik")
public class UserViewDto {


    @ApiModelProperty(value = "Nazwa użytkownika")
    @NotBlank
    private String username;


    public boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }

    @ApiModelProperty(value = "id logged")
    @NotBlank
    private boolean isLogged;

    @Enumerated
    private EnumSexUser sex;
    private String city;

    @Enumerated
    private EnumMaritalStatus maritalStatus;


    @ElementCollection
    @CollectionTable(name = "listOfUsers")
    private List<EnumLanguages> languagesList;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public EnumSexUser getSex() {
        return sex;
    }

    public void setSex(EnumSexUser sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EnumMaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(EnumMaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public List<EnumLanguages> getLanguagesList() {
        return languagesList;
    }

    public void setLanguagesList(List<EnumLanguages> languagesList) {
        this.languagesList = languagesList;
    }
}

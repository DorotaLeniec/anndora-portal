package pl.anndora.helloLove.model.DTOs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;
import pl.anndora.helloLove.model.Enum.EnumLanguages;
import pl.anndora.helloLove.model.Enum.EnumMaritalStatus;
import pl.anndora.helloLove.model.Enum.EnumSexUser;

@ApiModel(description = "Użytkownik")
public class UserAllDto {


    @ApiModelProperty(value = "Nazwa użytkownika")
    @NotBlank
    private String username;


    @ApiModelProperty(value = "id logged")
    @NotBlank
    private boolean isLogged;

    public boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }

    private EnumSexUser sex;
    private String city;

    private EnumMaritalStatus maritalStatus;
    private EnumLanguages Language;

    public EnumSexUser getSex() {
        return sex;
    }

    public void setSex(EnumSexUser sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EnumMaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(EnumMaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public EnumLanguages getLanguage() {
        return Language;
    }

    public void EnumLanguage(EnumLanguages language) {
        Language = language;
    }

}

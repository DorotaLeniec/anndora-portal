package pl.anndora.helloLove.exceptions;

public class AdminException extends RuntimeException {
   public AdminException(String msg){
       super(msg);
   }
}

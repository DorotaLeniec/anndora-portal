package pl.anndora.helloLove;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "pl.anndora.helloLove")
public class HelloLoveApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloLoveApplication.class, args);
	}
}

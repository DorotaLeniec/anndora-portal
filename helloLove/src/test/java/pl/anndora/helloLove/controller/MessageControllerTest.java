package pl.anndora.helloLove.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import pl.anndora.helloLove.mapper.MessageMapper;
import pl.anndora.helloLove.mapper.UserMapper;
import pl.anndora.helloLove.model.DTOs.LoginDto;
import pl.anndora.helloLove.model.DTOs.MessageDto;
import pl.anndora.helloLove.model.DTOs.MessageViewDto;
import pl.anndora.helloLove.model.DTOs.UserViewDto;
import pl.anndora.helloLove.model.Message;
import pl.anndora.helloLove.model.TokenDto;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.repository.UserRepository;
import pl.anndora.helloLove.service.MessageService;
import pl.anndora.helloLove.service.TokenAuthenticationService;
import pl.anndora.helloLove.service.UserService;
import sun.misc.resources.Messages;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageControllerTest {

    @InjectMocks
    private MessageController testObj = new MessageController();
    @Mock
    private MessageService messageService;
    @Mock
    private MessageMapper messageMapper;
    @Mock
    private UserService userService;
    @Mock
    private TokenAuthenticationService tokenService;
    @Mock
    private HttpServletRequest request;

    @Mock
    private User user;
    @Mock
    private MessageDto messageDto = new MessageDto();
    @Mock
    private MessageViewDto messageViewDto;

    private String username = "username";
    private Optional<String> ctoken = Optional.of("username_password");
    private Long id = 12L;


    @Test
    public void shouldReturn401StatusWhenAccessDenied() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(Optional.empty());

        ResponseEntity<Void> result = testObj.sendMessage(messageDto, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(401);

    }

    @Test
    public void shouldReturn404StatusWhenReceiverNotFound() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(tokenService.getUserNameFromToken(ctoken.get())).thenReturn(username);
        when(userService.findByUsername(username)).thenReturn(user);
        when(messageDto.getReceiverName()).thenReturn(username);
        when(userService.findByUsername(username)).thenReturn(null);

        ResponseEntity<Void> result = testObj.sendMessage(messageDto, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    public void shouldReturn200StatusWhenMessageSent() {
        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(tokenService.getUserNameFromToken(ctoken.get())).thenReturn(username);
        when(userService.findByUsername(username)).thenReturn(user);
        when(messageDto.getReceiverName()).thenReturn(username);
        when(userService.findByUsername(username)).thenReturn(user);

        ResponseEntity<Void> result = testObj.sendMessage(messageDto, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        verify(messageService).sendMessage((messageMapper.map(messageDto, user)));
    }

    @Test
    public void shouldReturn404StatusWhenMessageNotFound() {
        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(messageService.showMessage(id)).thenReturn(Optional.empty());

        ResponseEntity<MessageViewDto> result = testObj.showMessage(id, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(404);
        assertThat(result.getBody()).isNull();
    }

    @Test
    public void shouldReturn401StatusWhenTokenNotFound() {
        when(tokenService.getCurrentUserToken(request)).thenReturn(Optional.empty());

        ResponseEntity<MessageViewDto> result = testObj.showMessage(id, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(401);
        assertThat(result.getBody()).isNull();
        verifyNoMoreInteractions(messageService);
    }

    @Test
    public void shouldReturn200StatusWhenShowMessage() {
        Optional<Message> messageOptional = Optional.of(new Message());
        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(messageService.showMessage(id)).thenReturn(messageOptional);
        when(messageMapper.map(messageOptional.get())).thenReturn(messageViewDto);

        ResponseEntity<MessageViewDto> result = testObj.showMessage(id, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody()).isNotNull();
    }


}
package pl.anndora.helloLove.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import pl.anndora.helloLove.model.DTOs.LoginDto;
import pl.anndora.helloLove.model.DTOs.UserDto;
import pl.anndora.helloLove.model.DTOs.UserViewEditDto;
import pl.anndora.helloLove.model.TokenDto;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.repository.UserRepository;
import pl.anndora.helloLove.service.TokenAuthenticationService;
import pl.anndora.helloLove.service.UserService;


import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    private UserController testObj = new UserController();

    @Mock
    private UserService userService;

    @Mock
    private TokenAuthenticationService tokenService;

    @Mock
    HttpServletRequest request;

    @Mock
    private User user;

    @Mock
    private LoginDto loginDto = new LoginDto();
    @Mock
    private UserViewEditDto userVEDto;

    private String currentToken = "username_password";

    private String password = "password";
    private String username = "username";

    private Optional<String> ctoken = Optional.of("username_password");

    @Captor
    ArgumentCaptor<UsernamePasswordAuthenticationToken> argumentCaptor;

    //@Mock
    //TokenDto tokenDto;

    //Login tests

    @Test
    public void shouldReturn404StatusWhenUserIsNotFound() {

        when(userService.findByUsername(loginDto.getUsername())).thenReturn(null);

        ResponseEntity<TokenDto> result = testObj.loginProcess(loginDto);

        assertThat(result.getStatusCodeValue()).isEqualTo(404);
        assertThat(result.getBody()).isNull();
        verify(tokenService).authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        //verifyNoMoreInteractions(tokenService);

    }

    @Test
    public void shouldReturn200StatusWhenTokenIsNotEmpty() {
        TokenDto tokenDto = new TokenDto(currentToken);

        when(userService.findByUsername(loginDto.getUsername())).thenReturn(user);

        when(user.getPassword()).thenReturn(password);
        when(loginDto.getPassword()).thenReturn(password);
        when(tokenService.authenticate(new UsernamePasswordAuthenticationToken(
                loginDto.getUsername(), loginDto.getPassword()))).thenReturn(currentToken);


        ResponseEntity<TokenDto> result = testObj.loginProcess(loginDto);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody()).isEqualToComparingFieldByField(tokenDto);
        verify(tokenService).authenticate(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualToComparingFieldByField(new UsernamePasswordAuthenticationToken(
                loginDto.getUsername(), loginDto.getPassword()));
        verifyNoMoreInteractions(tokenService);
    }

    //Logout tests
    @Test
    public void shouldReturn200StatusWhenUserIsLoggedOut() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(tokenService.getUserNameFromToken(ctoken.get())).thenReturn(username);
        when(userService.findByUsername(username)).thenReturn(user);

        ResponseEntity<TokenDto> result = testObj.logoutProcess(request);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(user.isLogged()).isEqualTo(false);

        verify(userService).logout(user);
    }

    @Test
    public void shouldReturn404StatusWhenUserCannotBeLoggedOut() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(Optional.empty());

        ResponseEntity<TokenDto> result = testObj.logoutProcess(request);

        assertThat(result.getStatusCodeValue()).isEqualTo(404);

    }

    //Delete tests
    @Test
    public void shouldReturnStatus204WhenUserIsDeleted() {

        when(userService.findByUsername(username)).thenReturn(user);

        ResponseEntity<Void> result = testObj.deleteProcess(username);

        assertThat(result.getStatusCodeValue()).isEqualTo(204);
        verify(userService).deleteUserByUsername(username);
    }

    @Test
    public void shouldReturnStatus401WhenUserCannotBeDeleted() {

        when(userService.findByUsername(username)).thenReturn(null);

        ResponseEntity<Void> result = testObj.deleteProcess(username);

        assertThat(result.getStatusCodeValue()).isEqualTo(401);

    }

//    @Test
//    public void shouldReturnStatus403WhenAccessDenied(){
//
//        when(userService.findByUsername(username)).thenReturn(null);
//
//        ResponseEntity<Void> result = testObj.deleteProcess(username);
//
//        assertThat(result.getStatusCodeValue()).isEqualTo(401);
//    }


    //Edit
    @Test
    public void shouldReturnStatus200WhenUserEdited() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(tokenService.getUserNameFromToken(ctoken.get())).thenReturn(username);

        ResponseEntity<Void> result = testObj.editUser(userVEDto, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        verify(userService).editUser(userVEDto, username);
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void shouldReturnStatus404WhenUserCannotBeEdited() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(Optional.empty());

        ResponseEntity<Void> result = testObj.editUser(userVEDto, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(404);

        //verify(userService).editUser(userVEDto, username);
        verifyNoMoreInteractions(userService);
    }
}

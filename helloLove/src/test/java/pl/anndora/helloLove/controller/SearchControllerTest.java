package pl.anndora.helloLove.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import pl.anndora.helloLove.mapper.UserMapper;
import pl.anndora.helloLove.model.DTOs.LoginDto;
import pl.anndora.helloLove.model.DTOs.UserViewDto;
import pl.anndora.helloLove.model.DTOs.UserViewEditDto;
import pl.anndora.helloLove.model.TokenDto;
import pl.anndora.helloLove.model.User;
import pl.anndora.helloLove.repository.UserRepository;
import pl.anndora.helloLove.service.TokenAuthenticationService;
import pl.anndora.helloLove.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SearchControllerTest {

    @InjectMocks
    private SearchController testObj = new SearchController();

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserService userService;

    @Mock
    private TokenAuthenticationService tokenService;

    @Mock
    HttpServletRequest request;

    @Mock
    private User user;

    @Mock
    private LoginDto loginDto = new LoginDto();

    @Mock
    private UserMapper userMapper;
    @Mock
    private UserViewDto foundViewDto;


    private String currentToken = "username_password";
    private String adminToken = "admin_admin";

    private String password = "password";
    private String username = "username";

    private Optional<String> ctoken = Optional.of("username_password");

    @Captor
    ArgumentCaptor<UsernamePasswordAuthenticationToken> argumentCaptor;


    @Test
    public void shouldReturn401StatusWhenAccessDenied() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(Optional.empty());
        when(userService.findByUsername(username)).thenReturn(user);

        ResponseEntity<TokenDto> result = testObj.findUserByUsername(username, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(401);
        assertThat(result.getBody()).isNull();
        verify(userService).findByUsername(username);
        verifyNoMoreInteractions(userService);
    }


    @Test
    public void shouldReturn404StatusWhenUserIsNotFound() {

        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(userService.findByUsername(username)).thenReturn(null);

        ResponseEntity<TokenDto> result = testObj.findUserByUsername(username, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(404);
        assertThat(result.getBody()).isNull();
        verify(userService).findByUsername(username);
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void shouldReturn200StatusWhenUserIsFound() {
        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(userService.findByUsername(username)).thenReturn(user);
        when(user.isAdmin()).thenReturn(false);
        when(userMapper.map(user)).thenReturn(foundViewDto);

        ResponseEntity<TokenDto> result = testObj.findUserByUsername(username, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody()).isEqualToComparingFieldByField(foundViewDto);
        verify(userService).findByUsername(username);
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void shouldReturn200StatusWhenUserIsFoundByAdmin() {
        when(tokenService.getCurrentUserToken(request)).thenReturn(ctoken);
        when(userService.findByUsername(username)).thenReturn(user);
        when(user.isAdmin()).thenReturn(true);

        ResponseEntity<TokenDto> result = testObj.findUserByUsername(username, request);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody()).isEqualToComparingFieldByField(user);
        verify(userService).findByUsername(username);
        verifyNoMoreInteractions(userService);
    }


}
